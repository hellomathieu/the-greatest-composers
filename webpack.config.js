module.exports = {
  entry: "./src/index.js",
  output: {
    path: "./public",
    filename: "index.js"
  },
  devServer: {
    contentBase: "./public",
    inline: true,
    port: 3333
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel",
        query: {
          presets: ["react", "es2015", "stage-0"],
        },
      },

      {
        test: /\.css$/,
        loaders: ["style", "css"],
      },
    ],
  }
}
