# The Greatest Composers

## Install

- `npm i -g json-server`

- `npm i`

## Start

- Launch the json server
`json-server --watch src/mocks/db.json --port 4000`

- Launch the dev server
`npm start`

- Open your favorite browser and go to :
`http://localhost:3333`

## API EndPoints

The database is located at : `src/mocks/db.json`.

- Fetch all composers :
`GET http://localhost:4000/composers`

- Fetch a specific composer :
`GET http://localhost:4000/composers/:id`


